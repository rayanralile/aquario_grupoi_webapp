﻿namespace AquarioSite.Models
{
    public class Dados
    {
        public int Id { get; set; }

        public float Temperatura { get; set; }
        public float Tds { get; set; }
        public float Ph { get; set; }

        public bool IsVccAtivo { get; set; }

        public DateTime DataHora { get; set; }

        public Dados()
        {
            this.DataHora = DateTime.UtcNow;
        }
    }
}
