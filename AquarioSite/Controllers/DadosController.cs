﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AquarioSite.Data;
using AquarioSite.Models;
using Newtonsoft.Json;

namespace AquarioSite.Controllers
{
    public class DadosController : Controller
    {
        private readonly AquarioSiteContext _context;

        HttpClientHandler _clientHandler = new HttpClientHandler();

        Dados _oDados = new Dados();
        List<Dados> _ListDados = new List<Dados>();
        public DadosController(AquarioSiteContext context)
        {
            _clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            _context = context;
        }

        // GET: Dados
        public async Task<IActionResult> Index()
        {
            _ListDados = new List<Dados>();
            using (var httpClient = new HttpClient(_clientHandler))
            {
                using (var response = await httpClient.GetAsync("https://grupoi.azurewebsites.net/api/dados"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    _ListDados = JsonConvert.DeserializeObject<List<Dados>>(apiResponse);

                }
            }
            return View(_ListDados.OrderByDescending(m => m.Id).Take(100));
            //return _context.Dados != null ?
            //                View(await _context.Dados.ToListAsync()) :
            //                Problem("Entity set 'AquarioSiteContext.Dados'  is null.");
        }

        // GET: Dados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            //  if (id == null || _context.Dados == null)
            //   {
            //       return NotFound();
            //  }

            //   var dados = await _context.Dados
            //       .FirstOrDefaultAsync(m => m.Id == id);
            //    if (dados == null)
            //   {
            //        return NotFound();
            //    }
            _ListDados = new List<Dados>();
            using (var httpClient = new HttpClient(_clientHandler))
            {
                using (var response = await httpClient.GetAsync("https://grupoi.azurewebsites.net/api/dados"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    _ListDados = JsonConvert.DeserializeObject<List<Dados>>(apiResponse);

                }
            }
            _oDados = _ListDados.OrderByDescending(m => m.Id).FirstOrDefault();

            if (_oDados == null)
            {
                return NotFound();
            }else
            {
                if (_oDados.IsVccAtivo)
                    ViewBag.Power = true;
                else
                    ViewBag.Power = false;
                return View(_oDados);
            }
        }

        // GET: Dados/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Dados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Temperatura,Tds,Ph,IsVccAtivo,DataHora")] Dados dados)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dados);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dados);
        }

        // GET: Dados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Dados == null)
            {
                return NotFound();
            }

            var dados = await _context.Dados.FindAsync(id);
            if (dados == null)
            {
                return NotFound();
            }
            return View(dados);
        }

        // POST: Dados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Temperatura,Tds,Ph,IsVccAtivo,DataHora")] Dados dados)
        {
            if (id != dados.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dados);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DadosExists(dados.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dados);
        }

        // GET: Dados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Dados == null)
            {
                return NotFound();
            }

            var dados = await _context.Dados
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dados == null)
            {
                return NotFound();
            }

            return View(dados);
        }

        // POST: Dados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Dados == null)
            {
                return Problem("Entity set 'AquarioSiteContext.Dados'  is null.");
            }
            var dados = await _context.Dados.FindAsync(id);
            if (dados != null)
            {
                _context.Dados.Remove(dados);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DadosExists(int id)
        {
          return (_context.Dados?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
