﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using AquarioSite.Data;
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<AquarioSiteContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("AquarioSiteContext") ?? throw new InvalidOperationException("Connection string 'AquarioSiteContext' not found.")));

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Dados}/{action=Details}/{id?}");

app.Run();
