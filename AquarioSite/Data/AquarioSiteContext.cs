﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AquarioSite.Models;

namespace AquarioSite.Data
{
    public class AquarioSiteContext : DbContext
    {
        public AquarioSiteContext (DbContextOptions<AquarioSiteContext> options)
            : base(options)
        {
        }

        public DbSet<AquarioSite.Models.Dados>? Dados { get; set; }
    }
}
